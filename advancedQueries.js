// COMPARISON QUERY OPERATOR ---------------------------------------

// GREATER THAN OR EQUAL - greater than or equal to a specified value
db.users.find({ age: {$gt : 50}}); //greater than 50

db.users.find({ age: {$gte : 50}}); //greater than or equal to 50


// LESS THAN OR EQUAL - less than or equal to a specified value
db.users.find({ age: {$lt : 50}}); //less than 50

db.users.find({ age: {$lte : 50}}); //less than or equal to 50


// NOT EQUAL - find documents not equal to the specified value
db.users.find({ age: {$ne : 82}}); //not equal to 82

// SPECIFIC MATCH - in one field using different values
db.users.find( {lastName: {$in: ["Hawking", "Doe"]}});
db.users.find( {courses: {$in: ["React"]}});


// LOGICAL QUERY OPERATORS ---------------------------------------

// ($or) OR operator - either of the two or more specified values
db.users.find( {$or : [{firstName: "Neil"}, {age: "25"}]});
db.users.find( {$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});


// ($and) AND operator - matches all specified criteria
db.users.find( {$and: [{age: {$ne:82}}, {age: {$ne:76}}]});


// FIELD PROJECTION --------------------------------------------
// allows to include a specific value only when retrieving
// yung may value na 1 siya lang ang ididisplay na property
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
	}
);

// FIELD EXCLUSION --------------------------------------------
// yung may value na 0 siya naman i eexclude na property
db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact: 0,
		department: 0
	}
);

// Sample using combined projection and exlusion
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
);
// Another example (acessing document inside object) using dot operator
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
);

db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone": 0
	}
);

// SLICE OPERATOR -----------------------------------------------------
db.users.find(
	{ "namearr" : 
		{
		namea: "Juan" //starting index
		}
	},
	{
		namearr: {$slice: 1} //count ng iislice from array
	}
);



// EVALUATION QUERY OPERATORS

// ($regex) - regular expressions: find documents that match specific string pattern

// CASE SENSITIVE QUERY
db.users.find({firstName: {$regex: "N"}});

// CASE INSENSITIVE QUERY (just add $options '$i')
db.users.find({firstName: {$regex: 'j', $options: '$i'}});